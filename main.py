from PIL import Image
import os

def walkFolder(source_folder, target_folder, color_name, color):
    for dirname, dirnames, filenames in os.walk(source_folder):
        for filename in filenames:
            base_icon_path = os.path.join(dirname, filename)
            if '.png' in base_icon_path:
                custom_dirname = dirname.replace(source_folder, target_folder).replace("white", color_name)
                custom_filename = filename.replace(source_folder, target_folder).replace("white", color_name)

                try:
                    os.makedirs(custom_dirname)
                    break
                except OSError:
                    ""

                generateIcon(base_icon_path, os.path.join(custom_dirname, custom_filename), color)


def generateIcon(source, target, color):
    icon = Image.open(source)
    pixels = icon.load()
    icon_size = icon.size

    target_icon = Image.new(mode='RGBA',size=icon_size, color=(0,0,0,0)) #create the image object to be the final product
    target_pixels = target_icon.load();

    # print icon.mode
    if icon.mode == "1":
        for y in range(icon_size[0]):
            for x in range(icon_size[1]):
                if(pixels[x, y] == 255):
                    target_pixels[x, y] = (color[0],color[1],color[2], 255)
    elif icon.mode == "LA":
        for y in range(icon_size[0]):
            for x in range(icon_size[1]):
                if(pixels[x, y][0] == 255):
                    target_pixels[x, y] = (color[0],color[1],color[2], pixels[x, y][1])
    else:
        print icon.mode
    target_icon.save(target,'PNG')


walkFolder("src", "target", "orange", (236, 122, 0))